﻿namespace Skelp.Fizzsy.Lib
{
    /// <summary>
    /// Describes the operator used in a predicate to compare two operands.
    /// </summary>
    public enum Operator
    {
        /// <summary>
        /// Asserts operand-equality.
        /// </summary>
        Equals,

        /// <summary>
        /// Asserts that the left-operand is less than the right-operand.
        /// </summary>
        LessThan,

        /// <summary>
        /// Asserts that the left-operand is less than or equal to the right-operand.
        /// </summary>
        LessThanOrEqual,

        /// <summary>
        /// Asserts that the left-operand is greater than the right-operand.
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Asserts that the left-operand is greater than or equal to the right-operand.
        /// </summary>
        GreaterThanOrEqual,

        /// <summary>
        /// Asserts that the left-operand is a multiple of the right-operand with a remainder of <see cref="PredicateLogic{T}.ResultArg"/>.
        /// </summary>
        Modulo,
    }
}