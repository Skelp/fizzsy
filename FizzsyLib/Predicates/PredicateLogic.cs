﻿namespace Skelp.Fizzsy.Lib
{
    using System;

    /// <summary>
    /// Represents high-level information about a <see cref="Predicate{T}"/>.
    /// </summary>
    /// <typeparam name="T">The type used in the <see cref="Predicate{T}"/>.</typeparam>
    [Serializable]
    internal readonly struct PredicateLogic<T>
         where T : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PredicateLogic{T}"/> struct.
        /// </summary>
        /// <param name="rightOperand">Represents the right operand of the <see cref="Predicate{T}"/>.</param>
        /// <param name="opr">Determines the logical operator of the <see cref="Predicate{T}"/>.</param>
        /// <param name="resultArg">Is used for the operator <see cref="Operator.Modulo"/> and unused for any other.</param>
        /// <param name="negate">If true will negate the output of the predicate.</param>
        public PredicateLogic(T rightOperand, Operator opr, T? resultArg, bool negate)
        {
            RightOperand = rightOperand;
            Operator = opr;
            ResultArg = resultArg;
            Negate = negate;
        }

        /// <summary>
        /// Gets the right operand of the <see cref="Predicate{T}"/>.
        /// </summary>
        public T RightOperand { get; }

        /// <summary>
        /// Gets the logical operator of the <see cref="Predicate{T}"/>.
        /// </summary>
        public Operator Operator { get; }

        /// <summary>
        /// Gets the result used for the operator <see cref="Operator.Modulo"/> while unused for any other.
        /// </summary>
        public T? ResultArg { get; }

        /// <summary>
        /// Gets a value indicating whether or not to negate the output of the <see cref="Predicate{T}"/>.
        /// </summary>
        public bool Negate { get; }
    }
}