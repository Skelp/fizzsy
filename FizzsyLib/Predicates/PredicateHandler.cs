﻿namespace Skelp.Fizzsy.Lib
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    /// A handler designed to compile and serialize predicates at runtime.
    /// </summary>
    /// <typeparam name="T">Any numeric type.</typeparam>
    /// <remarks>The predicates created by the class are very limited and can only contain one operation at a time.</remarks>
    public class PredicateHandler<T>
        where T : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PredicateHandler{T}"/> class and compiles a <see cref="Predicate{T}"/> based on the given parameters.
        /// </summary>
        /// <param name="rightOperand">Represents the right operand of the <see cref="Predicate{T}"/>.</param>
        /// <param name="opr">Determines the logical operator of the <see cref="Predicate{T}"/>.</param>
        /// <param name="resultArg">Is used for the operator <see cref="Operator.Modulo"/> and unused for any other.</param>
        /// <param name="negate">If true will negate the output of the predicate.</param>
        /// <exception cref="ArgumentException">Thrown when <typeparamref name="T"/> is not a numeric type.</exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="opr"/> is equal to <see cref="Operator.Modulo"/> and <paramref name="resultArg"/> is null.</exception>
        /// <example>
        /// Predicate to be created:
        /// <code>
        /// i => i % 3 == 0
        /// </code>
        /// Matching constructor call:
        /// <code lang="c#">
        /// var predicateHandler = new PredicateHandler&lt;int&gt;(3, Operator.Modulo, null, false);
        /// </code>
        /// </example>
        public PredicateHandler(T rightOperand, Operator opr, T? resultArg, bool negate)
        {
            if (typeof(T).IsNumber() == false)
            {
                throw new ArgumentException($"Generic type parameter '{nameof(T)}' must be a number!");
            }

            PredicateLogic = new PredicateLogic<T>(rightOperand, opr, resultArg, negate);
            CompilePredicate();
        }

        /// <summary>
        /// Gets the compiled <see cref="Predicate{T}"/>.
        /// </summary>
        public Predicate<T> Predicate { get; private set; }

        /// <summary>
        /// Gets high-level, minimal information used as a template for compiling and serializing.
        /// </summary>
        internal PredicateLogic<T> PredicateLogic { get; private set; }

        /// <summary>
        /// Compiles a <see cref="Predicate{T}"/> based on parameters passed into the constructor.
        /// </summary>
        private void CompilePredicate()
        {
            ParameterExpression leftArgParam = Expression.Parameter(typeof(T), "leftArg");
            ConstantExpression rightArgParam = Expression.Constant(PredicateLogic.RightOperand, typeof(T));
            BinaryExpression equalsExpression = null;

            switch (PredicateLogic.Operator)
            {
                case Operator.Equals:
                    equalsExpression = PredicateLogic.Negate
                        ? Expression.NotEqual(leftArgParam, rightArgParam)
                        : Expression.Equal(leftArgParam, rightArgParam);
                    break;

                case Operator.LessThan:
                    equalsExpression = PredicateLogic.Negate
                        ? Expression.GreaterThanOrEqual(leftArgParam, rightArgParam)
                        : Expression.LessThan(leftArgParam, rightArgParam);
                    break;

                case Operator.GreaterThan:
                    equalsExpression = PredicateLogic.Negate
                        ? Expression.LessThanOrEqual(leftArgParam, rightArgParam)
                        : Expression.GreaterThan(leftArgParam, rightArgParam);
                    break;

                case Operator.GreaterThanOrEqual:
                    equalsExpression = PredicateLogic.Negate
                        ? Expression.LessThan(leftArgParam, rightArgParam)
                        : Expression.GreaterThanOrEqual(leftArgParam, rightArgParam);
                    break;

                case Operator.LessThanOrEqual:
                    equalsExpression = PredicateLogic.Negate
                        ? Expression.GreaterThan(leftArgParam, rightArgParam)
                        : Expression.LessThanOrEqual(leftArgParam, rightArgParam);
                    break;

                case Operator.Modulo:
                    if (PredicateLogic.ResultArg.HasValue == false)
                    {
                        var msg = $"'{nameof(PredicateLogic.ResultArg)}' cannot be null when {nameof(Operator)} '{nameof(PredicateLogic.Operator)}' is '{PredicateLogic.Operator}'!";
                        throw new ArgumentNullException(msg);
                    }

                    ConstantExpression equalsToArgParam = Expression.Constant(PredicateLogic.ResultArg, typeof(T));
                    var moduloExpression = Expression.Modulo(leftArgParam, rightArgParam);

                    equalsExpression = PredicateLogic.Negate
                        ? Expression.NotEqual(moduloExpression, equalsToArgParam)
                        : Expression.Equal(moduloExpression, equalsToArgParam);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(PredicateLogic.Operator));
            }

            var expression = Expression.Lambda<Predicate<T>>(equalsExpression, new ParameterExpression[] { leftArgParam });
            Predicate = expression.Compile();
        }
    }
}