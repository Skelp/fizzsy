﻿namespace Skelp.Fizzsy.Lib
{
    using System;

    /// <summary>
    /// Contains extention methods for the <see cref="Type"/> class.
    /// </summary>
    internal static class TypeExtentions
    {
        /// <summary>
        /// Ensures the given <paramref name="type"/> is a number.
        /// </summary>
        /// <param name="type">Any <see cref="Type"/>.</param>
        /// <returns>
        /// Returns true if <paramref name="type"/> is a number, returns false otherwise.
        /// </returns>
        public static bool IsNumber(this Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }
    }
}