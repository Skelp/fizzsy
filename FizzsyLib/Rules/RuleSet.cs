﻿namespace Skelp.Fizzsy.Lib
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Represents an abstract container for instances of type <see cref="Rule{T}"/> which in turn can be used to process Fizzsy logic.
    /// </summary>
    /// <typeparam name="T">Any numeric type.</typeparam>
    public class RuleSet<T> : IEnumerable<Rule<T>>
        where T : struct
    {
        private static Regex regex = new Regex(@"(?<var>\w+) *(?<operator>[%<>=][=]?) *(?<rightOperand>\d+([\.,]\d+)?) *(?<negate>!)?=? *(?<result>\d+([\.,]\d+)?)?", RegexOptions.Compiled);

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSet{T}"/> class.
        /// </summary>
        public RuleSet()
        {
            RuleLogics = new ObservableCollection<RuleLogic<T>>();
            Rules = new ObservableCollection<Rule<T>>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSet{T}"/> classs and compiles a <see cref="Predicate{T}"/> for each item in <paramref name="predicateHandlers"/>.
        /// </summary>
        /// <param name="predicateHandlers">Any collection of <see cref="RuleLogic{T}"/> where no item is null.</param>
        public RuleSet(IEnumerable<RuleLogic<T>> predicateHandlers)
        {
            RuleLogics = new ObservableCollection<RuleLogic<T>>(predicateHandlers);
            Rules = new ObservableCollection<Rule<T>>();

            foreach (var item in predicateHandlers)
            {
                Rules.Add(ConvertToRule(item));
            }
        }

        /// <summary>
        ///  Gets or sets the ordered collection of <see cref="Rule{T}"/> instances.
        /// </summary>
        private ObservableCollection<Rule<T>> Rules { get; set; }

        /// <summary>
        ///  Gets or sets the ordered collection of <see cref="RuleLogic{T}"/> instances.
        /// </summary>
        private ObservableCollection<RuleLogic<T>> RuleLogics { get; set; }

        /// <summary>
        ///  Gets or sets a <see cref="Rule{T}"/>.
        /// </summary>
        /// <param name="index">Any positive integer.</param>
        /// <returns>
        /// Returns the <see cref="Rule{T}"/> corresponding to the <paramref name="index"/>.
        /// </returns>
        public Rule<T> this[int index]
        {
            get
            {
                return Rules[index];
            }

            set
            {
                Rules[index] = value;
            }
        }

        /// <summary>
        /// Takes a <see cref="byte"/> array containing a serialized <see cref="RuleSet{T}"/> and creates a new, deserialized instance.
        /// </summary>
        /// <param name="data">Any valid, serialized <see cref="RuleSet{T}"/>.</param>
        /// <returns>
        /// Returns a new, deserialized <see cref="RuleSet{T}"/> instance.
        /// </returns>
        public static RuleSet<T> Deserialize(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, 0, data.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var formatter = new BinaryFormatter();
                formatter.Binder = new RuleSetBinder<T>();
                var predList = (List<KeyValuePair<PredicateLogic<T>, string>>)formatter.Deserialize(ms);

                var compiledRules = new List<RuleLogic<T>>();

                foreach (var item in predList)
                {
                    var predLogic = new PredicateHandler<T>(item.Key.RightOperand, item.Key.Operator, item.Key.ResultArg, item.Key.Negate);
                    compiledRules.Add(new RuleLogic<T>(predLogic, item.Value));
                }

                return new RuleSet<T>(compiledRules);
            }
        }

        /// <summary>
        /// Serializes the current <see cref="RuleSet{T}"/> instance into a <see cref="byte"/> array.
        /// </summary>
        /// <returns>
        /// Returns a <see cref="byte"/> array containing a serialized <see cref="RuleSet{T}"/> instance.
        /// </returns>
        public byte[] Serialize()
        {
            var tempList = new List<KeyValuePair<PredicateLogic<T>, string>>();

            foreach (var item in RuleLogics)
            {
                var predicatePair = new KeyValuePair<PredicateLogic<T>, string>(item.PredicateHandler.PredicateLogic, item.TrueValue);
                tempList.Add(predicatePair);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Binder = new RuleSetBinder<T>();
                formatter.Serialize(ms, tempList);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Initializes a new <see cref="Rule{T}"/> and adds it to the <see cref="RuleSet{T}"/> instance.
        /// </summary>
        /// <param name="rightOperand">Represents the right operand of the <see cref="Predicate{T}"/>.</param>
        /// <param name="opr">Determines the logical operator of the <see cref="Predicate{T}"/>.</param>
        /// <param name="resultArg">Is used for the operator <see cref="Operator.Modulo"/> and unused for any other.</param>
        /// <param name="negate">If true will negate the output of the predicate.</param>
        /// <param name="trueValue">Any <see cref="string"/> which will be returned when the corresponding <see cref="Predicate{T}"/> returns true.</param>
        public void Add(T rightOperand, Operator opr, T? resultArg, bool negate, string trueValue)
        {
            var predHandler = new PredicateHandler<T>(rightOperand, opr, resultArg, negate);
            Add(predHandler, trueValue);
        }

        /// <summary>
        /// Initializes a new <see cref="Rule{T}"/> and adds it to the <see cref="RuleSet{T}"/> instance.
        /// </summary>
        /// <param name="predicateHandler">Any valid instance of a <see cref="PredicateHandler{T}"/> class.</param>
        /// <param name="trueValue">Any <see cref="string"/> which will be returned when the corresponding <see cref="Predicate{T}"/> returns true.</param>
        public void Add(PredicateHandler<T> predicateHandler, string trueValue)
        {
            var predHandlerPair = new RuleLogic<T>(predicateHandler, trueValue);
            RuleLogics.Add(predHandlerPair);

            Rules.Add(ConvertToRule(predHandlerPair));
        }

        /// <summary>
        /// Initializes a new <see cref="Rule{T}"/> and adds it to the <see cref="RuleSet{T}"/> instance.
        /// <para>
        /// <paramref name="predicate"/> may be formatted as such where 'i' can be any variable name:
        /// "i%0=0", "i % 0 = 0", "i > 7"
        /// </para>
        /// </summary>
        /// <param name="predicate">Any valid <see cref="string"/> formatted as seen in the documentation.</param>
        /// <param name="trueValue">Any <see cref="string"/> which will be returned when the corresponding <see cref="Predicate{T}"/> returns true.</param>
        public void Add(string predicate, string trueValue)
        {
            var predHandler = ParseToPredicateHandler(predicate);
            var predHandlerPair = new RuleLogic<T>(predHandler, trueValue);
            RuleLogics.Add(predHandlerPair);

            Rules.Add(ConvertToRule(predHandlerPair));
        }

        /// <summary>
        /// Removes a <see cref="Rule{T}"/> instance.
        /// </summary>
        /// <param name="rule">Any valid <see cref="Rule{T}"/> instance.</param>
        /// <returns>
        /// Returns true if removal was successful, false otherwise.
        /// </returns>
        public bool Remove(Rule<T> rule)
        {
            var ruleLogicIndex = Rules.IndexOf(rule);
            RuleLogics.RemoveAt(ruleLogicIndex);
            return Rules.Remove(rule);
        }

        /// <summary>
        /// Removes a <see cref="Rule{T}"/> instance.
        /// </summary>
        /// <param name="index">Any valid index.</param>
        public void RemoveAt(int index)
        {
            RuleLogics.RemoveAt(index);
            Rules.RemoveAt(index);
        }

        /// <summary>
        /// Moves a <see cref="Rule{T}"/> instance with an index of <paramref name="newIndex"/> to <paramref name="oldIndex"/>.
        /// </summary>
        /// <param name="oldIndex">The index of the <see cref="Rule{T}"/> instance to be moved.</param>
        /// <param name="newIndex">The index where the <see cref="Rule{T}"/> instance is supposed to be moved to.</param>
        public void Move(int oldIndex, int newIndex)
        {
            Rules.Move(oldIndex, newIndex);
            RuleLogics.Move(oldIndex, newIndex);
        }

        /// <summary>
        /// Iterates all <see cref="Rule{T}"/> instances and builds a <see cref="string"/> based on the results of each <see cref="Rule{T}.Predicate"/> with <paramref name="value"/> as input.
        /// </summary>
        /// <param name="value">Any valid <typeparamref name="T"/> instance.</param>
        /// <returns>
        /// Returns a <see cref="string"/> which either contains a concatenation of every <see cref="Rule{T}.TrueValue"/> where its <see cref="Predicate{T}"/> has returned true,
        /// or, if no <see cref="Predicate{T}"/> has returned true, will return <paramref name="value"/>.
        /// </returns>
        public string Evaluate(T value)
        {
            var result = string.Empty;

            foreach (var rule in this)
            {
                if (rule.Predicate(value))
                {
                    result += rule.TrueValue;
                }
            }

            if (result == string.Empty)
            {
                result = value.ToString();
            }

            return result;
        }

        /// <summary>
        /// Iterates all <see cref="Rule{T}"/> instances and builds a <see cref="string"/> based on the results of each <see cref="Rule{T}.Predicate"/> with <paramref name="values"/> as input.
        /// </summary>
        /// <param name="values">Any valid collection of <typeparamref name="T"/> instances.</param>
        /// <returns>
        /// Returns a <see cref="string"/> array where each <see cref="string"/> either contains a concatenation of every <see cref="Rule{T}.TrueValue"/>
        /// where its <see cref="Predicate{T}"/> has returned true, or, if no <see cref="Predicate{T}"/> has returned true, will return the corresponding item in <paramref name="values"/>.
        /// </returns>
        public string[] EvaluateMany(params T[] values)
        {
            var results = new string[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                results[i] = Evaluate(values[i]);
            }

            return results;
        }

        /// <summary>
        /// Iterates all <see cref="Rule{T}"/> instances and builds a <see cref="string"/> based on the results of each <see cref="Rule{T}.Predicate"/> with <paramref name="values"/> as input.
        /// </summary>
        /// <param name="values">Any valid collection of <typeparamref name="T"/> instances.</param>
        /// <returns>
        /// Returns a <see cref="string"/> array where each <see cref="string"/> either contains a concatenation of every <see cref="Rule{T}.TrueValue"/>
        /// where its <see cref="Predicate{T}"/> has returned true, or, if no <see cref="Predicate{T}"/> has returned true, will return the corresponding item in <paramref name="values"/>.
        /// </returns>
        public string[] EvaluateMany(IEnumerable<T> values)
        {
            return EvaluateMany(values.ToArray());
        }

        /// <summary>
        /// Iterates all <see cref="Rule{T}"/> instances and builds a <see cref="string"/> based on the results of each <see cref="Rule{T}.Predicate"/>
        /// with a range of <paramref name="count"/> numbers, starting from <paramref name="start"/> as input.
        /// </summary>
        /// <param name="start">The inclusive number from which to start iterating.</param>
        /// <param name="count">The total amount of numbers to iterate.</param>
        /// <returns>
        /// Returns a <see cref="string"/> array where each <see cref="string"/> either contains a concatenation of every <see cref="Rule{T}.TrueValue"/>
        /// where its <see cref="Predicate{T}"/> has returned true, or, if no <see cref="Predicate{T}"/> has returned true, will return the corresponding number.
        /// </returns>
        public string[] EvaluateMany(int start, int count)
        {
            var results = new string[count];

            for (int i = 0; i < count; i++)
            {
                results[i] = Evaluate((T)Convert.ChangeType(i + start, typeof(T)));
            }

            return results;
        }

        /// <summary>
        /// Gets the Enumerator for the <see cref="RuleSet{T}"/> class.
        /// </summary>
        /// <returns>
        /// Returns the Enumerator for the <see cref="RuleSet{T}"/> class.
        /// </returns>
        public IEnumerator<Rule<T>> GetEnumerator()
        {
            for (int i = 0; i < Rules.Count; i++)
            {
                yield return Rules[i];
            }
        }

        /// <summary>
        /// Gets the Enumerator for the <see cref="RuleSet{T}"/> class.
        /// </summary>
        /// <returns>
        /// Returns the Enumerator for the <see cref="RuleSet{T}"/> class.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < Rules.Count; i++)
            {
                yield return Rules[i];
            }
        }

        /// <summary>
        /// Takes a <see cref="RuleLogic{T}"/> instance as a base to create a <see cref="Rule{T}"/>.
        /// </summary>
        /// <param name="ruleLogic">Any valid <see cref="RuleLogic{T}"/>.</param>
        /// <returns>
        /// Returns a new <see cref="Rule{T}"/>.
        /// </returns>
        private Rule<T> ConvertToRule(RuleLogic<T> ruleLogic)
            => new Rule<T>(ruleLogic.PredicateHandler.Predicate, ruleLogic.TrueValue);

        /// <summary>
        /// Takes any <see cref="string"/> and attempts to convert it to a <see cref="PredicateHandler{T}"/>.
        /// <para>
        /// <paramref name="input"/> may be formatted as such where 'i' can be any variable name:
        /// "i%0=0", "i % 0 = 0", "i > 7"
        /// </para>
        /// </summary>
        /// <example>
        /// ParseToPredicateHandler("i % 0 = 0");
        /// </example>
        /// <param name="input">Any <see cref="string"/> formatted as seen in the documentation.</param>
        /// <returns></returns>
        private PredicateHandler<T> ParseToPredicateHandler(string input)
        {
            input = input.Trim();
            var results = regex.Match(input).Groups;

            var rightOperand = (T)Convert.ChangeType(results["rightOperand"].Value, typeof(T));
            var @operator = ParseOperator(results["operator"].Value);
            var negate = results["negate"].Success;
            T resultArg = default;

            if (@operator == Operator.Modulo)
            {
                resultArg = (T)Convert.ChangeType(results["result"].Value, typeof(T));
            }

            return new PredicateHandler<T>(rightOperand, @operator, resultArg, negate);
        }

        private static Operator ParseOperator(string input)
        {
            switch (input)
            {
                case "=":
                    return Operator.Equals;
                case "%":
                    return Operator.Modulo;
                case ">":
                    return Operator.GreaterThan;
                case "<":
                    return Operator.LessThan;
                case ">=":
                    return Operator.GreaterThanOrEqual;
                case "<=":
                    return Operator.LessThanOrEqual;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}