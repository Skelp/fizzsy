﻿namespace Skelp.Fizzsy.Lib
{
    /// <summary>
    /// Represents a combination of a <see cref="PredicateHandler{T}"/> and a true-value used for serialization and to initialize <see cref="Rule{T}"/> instances.
    /// </summary>
    /// <typeparam name="T">The type used in the <see cref="PredicateHandler{T}"/>.</typeparam>
    public readonly struct RuleLogic<T>
        where T : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuleLogic{T}"/> struct.
        /// </summary>
        /// <param name="predicateHandler">Any valid <see cref="PredicateHandler{T}"/>.</param>
        /// <param name="trueValue">Any <see cref="string"/>.</param>
        public RuleLogic(PredicateHandler<T> predicateHandler, string trueValue)
        {
            PredicateHandler = predicateHandler;
            TrueValue = trueValue;
        }

        /// <summary>
        /// Gets the <see cref="PredicateHandler{T}"/>.
        /// </summary>
        public PredicateHandler<T> PredicateHandler { get; }

        /// <summary>
        /// Gets the <see cref="TrueValue"/> used when <see cref="PredicateHandler.Predicate"/> returns true.
        /// </summary>
        public string TrueValue { get; }
    }
}