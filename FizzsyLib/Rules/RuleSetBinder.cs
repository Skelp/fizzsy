﻿namespace Skelp.Fizzsy.Lib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;

    sealed class RuleSetBinder<T> : SerializationBinder
        where T : struct
    {
        private readonly string[] types = null;

        public RuleSetBinder()
        {
            types = new string[]
            {
            typeof(List<KeyValuePair<PredicateLogic<T>, string>>).FullName,
            typeof(KeyValuePair<PredicateLogic<T>, string>).FullName,
            typeof(PredicateLogic<T>).FullName,
            typeof(T).FullName,
            typeof(Operator).FullName,
            };
        }

        public override Type BindToType(string assemblyName, string typeName)
        {
            if (!(types.Contains(typeName)))
            {
                throw new SerializationException($"Disallowed type was used.");
            }
            return Assembly.Load(assemblyName).GetType(typeName);
        }
    }
}