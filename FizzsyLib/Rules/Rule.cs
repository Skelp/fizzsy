﻿namespace Skelp.Fizzsy.Lib
{
    using System;

    /// <summary>
    /// Represents a combination of a <see cref="Predicate{T}"/> and a true-value.
    /// </summary>
    /// <typeparam name="T">The type used in the <see cref="Predicate{T}"/>.</typeparam>
    public readonly struct Rule<T>
        where T : struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Rule{T}"/> struct.
        /// </summary>
        /// <param name="predicate">Any valid <see cref="Predicate{T}"/>.</param>
        /// <param name="trueValue">Any <see cref="string"/>.</param>
        public Rule(Predicate<T> predicate, string trueValue)
        {
            Predicate = predicate;
            TrueValue = trueValue;
        }

        /// <summary>
        /// Gets the <see cref="Predicate{T}"/>.
        /// </summary>
        public Predicate<T> Predicate { get; }

        /// <summary>
        /// Gets the <see cref="TrueValue"/> used when <see cref="Predicate"/> returns true.
        /// </summary>
        public string TrueValue { get; }
    }
}