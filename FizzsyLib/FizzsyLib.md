<a name='assembly'></a>
# FizzsyLib

## Contents

- [Operator](#T-Skelp-Fizzsy-Lib-Operator 'Skelp.Fizzsy.Lib.Operator')
  - [Equals](#F-Skelp-Fizzsy-Lib-Operator-Equals 'Skelp.Fizzsy.Lib.Operator.Equals')
  - [GreaterThan](#F-Skelp-Fizzsy-Lib-Operator-GreaterThan 'Skelp.Fizzsy.Lib.Operator.GreaterThan')
  - [GreaterThanOrEqual](#F-Skelp-Fizzsy-Lib-Operator-GreaterThanOrEqual 'Skelp.Fizzsy.Lib.Operator.GreaterThanOrEqual')
  - [LessThan](#F-Skelp-Fizzsy-Lib-Operator-LessThan 'Skelp.Fizzsy.Lib.Operator.LessThan')
  - [LessThanOrEqual](#F-Skelp-Fizzsy-Lib-Operator-LessThanOrEqual 'Skelp.Fizzsy.Lib.Operator.LessThanOrEqual')
  - [Modulo](#F-Skelp-Fizzsy-Lib-Operator-Modulo 'Skelp.Fizzsy.Lib.Operator.Modulo')
- [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1')
  - [#ctor(rightOperand,opr,resultArg,negate)](#M-Skelp-Fizzsy-Lib-PredicateHandler`1-#ctor-`0,Skelp-Fizzsy-Lib-Operator,System-Nullable{`0},System-Boolean- 'Skelp.Fizzsy.Lib.PredicateHandler`1.#ctor(`0,Skelp.Fizzsy.Lib.Operator,System.Nullable{`0},System.Boolean)')
  - [Predicate](#P-Skelp-Fizzsy-Lib-PredicateHandler`1-Predicate 'Skelp.Fizzsy.Lib.PredicateHandler`1.Predicate')
  - [PredicateLogic](#P-Skelp-Fizzsy-Lib-PredicateHandler`1-PredicateLogic 'Skelp.Fizzsy.Lib.PredicateHandler`1.PredicateLogic')
  - [CompilePredicate()](#M-Skelp-Fizzsy-Lib-PredicateHandler`1-CompilePredicate 'Skelp.Fizzsy.Lib.PredicateHandler`1.CompilePredicate')
- [PredicateLogic\`1](#T-Skelp-Fizzsy-Lib-PredicateLogic`1 'Skelp.Fizzsy.Lib.PredicateLogic`1')
  - [#ctor(rightOperand,opr,resultArg,negate)](#M-Skelp-Fizzsy-Lib-PredicateLogic`1-#ctor-`0,Skelp-Fizzsy-Lib-Operator,System-Nullable{`0},System-Boolean- 'Skelp.Fizzsy.Lib.PredicateLogic`1.#ctor(`0,Skelp.Fizzsy.Lib.Operator,System.Nullable{`0},System.Boolean)')
  - [Negate](#P-Skelp-Fizzsy-Lib-PredicateLogic`1-Negate 'Skelp.Fizzsy.Lib.PredicateLogic`1.Negate')
  - [Operator](#P-Skelp-Fizzsy-Lib-PredicateLogic`1-Operator 'Skelp.Fizzsy.Lib.PredicateLogic`1.Operator')
  - [ResultArg](#P-Skelp-Fizzsy-Lib-PredicateLogic`1-ResultArg 'Skelp.Fizzsy.Lib.PredicateLogic`1.ResultArg')
  - [RightOperand](#P-Skelp-Fizzsy-Lib-PredicateLogic`1-RightOperand 'Skelp.Fizzsy.Lib.PredicateLogic`1.RightOperand')
- [RuleLogic\`1](#T-Skelp-Fizzsy-Lib-RuleLogic`1 'Skelp.Fizzsy.Lib.RuleLogic`1')
  - [#ctor(predicateHandler,trueValue)](#M-Skelp-Fizzsy-Lib-RuleLogic`1-#ctor-Skelp-Fizzsy-Lib-PredicateHandler{`0},System-String- 'Skelp.Fizzsy.Lib.RuleLogic`1.#ctor(Skelp.Fizzsy.Lib.PredicateHandler{`0},System.String)')
  - [PredicateHandler](#P-Skelp-Fizzsy-Lib-RuleLogic`1-PredicateHandler 'Skelp.Fizzsy.Lib.RuleLogic`1.PredicateHandler')
  - [TrueValue](#P-Skelp-Fizzsy-Lib-RuleLogic`1-TrueValue 'Skelp.Fizzsy.Lib.RuleLogic`1.TrueValue')
- [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1')
  - [#ctor()](#M-Skelp-Fizzsy-Lib-RuleSet`1-#ctor 'Skelp.Fizzsy.Lib.RuleSet`1.#ctor')
  - [#ctor(predicateHandlers)](#M-Skelp-Fizzsy-Lib-RuleSet`1-#ctor-System-Collections-Generic-IEnumerable{Skelp-Fizzsy-Lib-RuleLogic{`0}}- 'Skelp.Fizzsy.Lib.RuleSet`1.#ctor(System.Collections.Generic.IEnumerable{Skelp.Fizzsy.Lib.RuleLogic{`0}})')
  - [Item](#P-Skelp-Fizzsy-Lib-RuleSet`1-Item-System-Int32- 'Skelp.Fizzsy.Lib.RuleSet`1.Item(System.Int32)')
  - [RuleLogics](#P-Skelp-Fizzsy-Lib-RuleSet`1-RuleLogics 'Skelp.Fizzsy.Lib.RuleSet`1.RuleLogics')
  - [Rules](#P-Skelp-Fizzsy-Lib-RuleSet`1-Rules 'Skelp.Fizzsy.Lib.RuleSet`1.Rules')
  - [Add(rightOperand,opr,resultArg,negate,trueValue)](#M-Skelp-Fizzsy-Lib-RuleSet`1-Add-`0,Skelp-Fizzsy-Lib-Operator,System-Nullable{`0},System-Boolean,System-String- 'Skelp.Fizzsy.Lib.RuleSet`1.Add(`0,Skelp.Fizzsy.Lib.Operator,System.Nullable{`0},System.Boolean,System.String)')
  - [Add(predicateHandler,trueValue)](#M-Skelp-Fizzsy-Lib-RuleSet`1-Add-Skelp-Fizzsy-Lib-PredicateHandler{`0},System-String- 'Skelp.Fizzsy.Lib.RuleSet`1.Add(Skelp.Fizzsy.Lib.PredicateHandler{`0},System.String)')
  - [ConvertToRule(ruleLogic)](#M-Skelp-Fizzsy-Lib-RuleSet`1-ConvertToRule-Skelp-Fizzsy-Lib-RuleLogic{`0}- 'Skelp.Fizzsy.Lib.RuleSet`1.ConvertToRule(Skelp.Fizzsy.Lib.RuleLogic{`0})')
  - [Deserialize(data)](#M-Skelp-Fizzsy-Lib-RuleSet`1-Deserialize-System-Byte[]- 'Skelp.Fizzsy.Lib.RuleSet`1.Deserialize(System.Byte[])')
  - [Evaluate(value)](#M-Skelp-Fizzsy-Lib-RuleSet`1-Evaluate-`0- 'Skelp.Fizzsy.Lib.RuleSet`1.Evaluate(`0)')
  - [EvaluateMany(values)](#M-Skelp-Fizzsy-Lib-RuleSet`1-EvaluateMany-`0[]- 'Skelp.Fizzsy.Lib.RuleSet`1.EvaluateMany(`0[])')
  - [EvaluateMany(values)](#M-Skelp-Fizzsy-Lib-RuleSet`1-EvaluateMany-System-Collections-Generic-IEnumerable{`0}- 'Skelp.Fizzsy.Lib.RuleSet`1.EvaluateMany(System.Collections.Generic.IEnumerable{`0})')
  - [EvaluateMany(start,count)](#M-Skelp-Fizzsy-Lib-RuleSet`1-EvaluateMany-System-Int32,System-Int32- 'Skelp.Fizzsy.Lib.RuleSet`1.EvaluateMany(System.Int32,System.Int32)')
  - [GetEnumerator()](#M-Skelp-Fizzsy-Lib-RuleSet`1-GetEnumerator 'Skelp.Fizzsy.Lib.RuleSet`1.GetEnumerator')
  - [Move(oldIndex,newIndex)](#M-Skelp-Fizzsy-Lib-RuleSet`1-Move-System-Int32,System-Int32- 'Skelp.Fizzsy.Lib.RuleSet`1.Move(System.Int32,System.Int32)')
  - [Serialize()](#M-Skelp-Fizzsy-Lib-RuleSet`1-Serialize 'Skelp.Fizzsy.Lib.RuleSet`1.Serialize')
  - [System#Collections#IEnumerable#GetEnumerator()](#M-Skelp-Fizzsy-Lib-RuleSet`1-System#Collections#IEnumerable#GetEnumerator 'Skelp.Fizzsy.Lib.RuleSet`1.System#Collections#IEnumerable#GetEnumerator')
- [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1')
  - [#ctor(predicate,trueValue)](#M-Skelp-Fizzsy-Lib-Rule`1-#ctor-System-Predicate{`0},System-String- 'Skelp.Fizzsy.Lib.Rule`1.#ctor(System.Predicate{`0},System.String)')
  - [Predicate](#P-Skelp-Fizzsy-Lib-Rule`1-Predicate 'Skelp.Fizzsy.Lib.Rule`1.Predicate')
  - [TrueValue](#P-Skelp-Fizzsy-Lib-Rule`1-TrueValue 'Skelp.Fizzsy.Lib.Rule`1.TrueValue')
- [TypeExtentions](#T-Skelp-Fizzsy-Lib-TypeExtentions 'Skelp.Fizzsy.Lib.TypeExtentions')
  - [IsNumber(type)](#M-Skelp-Fizzsy-Lib-TypeExtentions-IsNumber-System-Type- 'Skelp.Fizzsy.Lib.TypeExtentions.IsNumber(System.Type)')

<a name='T-Skelp-Fizzsy-Lib-Operator'></a>
## Operator `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

Describes the operator used in a predicate to compare two operands.

<a name='F-Skelp-Fizzsy-Lib-Operator-Equals'></a>
### Equals `constants`

##### Summary

Asserts operand-equality.

<a name='F-Skelp-Fizzsy-Lib-Operator-GreaterThan'></a>
### GreaterThan `constants`

##### Summary

Asserts that the left-operand is greater than the right-operand.

<a name='F-Skelp-Fizzsy-Lib-Operator-GreaterThanOrEqual'></a>
### GreaterThanOrEqual `constants`

##### Summary

Asserts that the left-operand is greater than or equal to the right-operand.

<a name='F-Skelp-Fizzsy-Lib-Operator-LessThan'></a>
### LessThan `constants`

##### Summary

Asserts that the left-operand is less than the right-operand.

<a name='F-Skelp-Fizzsy-Lib-Operator-LessThanOrEqual'></a>
### LessThanOrEqual `constants`

##### Summary

Asserts that the left-operand is less than or equal to the right-operand.

<a name='F-Skelp-Fizzsy-Lib-Operator-Modulo'></a>
### Modulo `constants`

##### Summary

Asserts that the left-operand is a multiple of the right-operand with a remainder of [ResultArg](#P-Skelp-Fizzsy-Lib-PredicateLogic`1-ResultArg 'Skelp.Fizzsy.Lib.PredicateLogic`1.ResultArg').

<a name='T-Skelp-Fizzsy-Lib-PredicateHandler`1'></a>
## PredicateHandler\`1 `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

A handler designed to compile and serialize predicates at runtime.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| T | Any numeric type. |

##### Remarks

The predicates created by the class are very limited and can only contain one operation at a time.

<a name='M-Skelp-Fizzsy-Lib-PredicateHandler`1-#ctor-`0,Skelp-Fizzsy-Lib-Operator,System-Nullable{`0},System-Boolean-'></a>
### #ctor(rightOperand,opr,resultArg,negate) `constructor`

##### Summary

Initializes a new instance of the [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1') class and compiles a [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') based on the given parameters.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| rightOperand | [\`0](#T-`0 '`0') | Represents the right operand of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| opr | [Skelp.Fizzsy.Lib.Operator](#T-Skelp-Fizzsy-Lib-Operator 'Skelp.Fizzsy.Lib.Operator') | Determines the logical operator of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| resultArg | [System.Nullable{\`0}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Nullable 'System.Nullable{`0}') | Is used for the operator [Modulo](#F-Skelp-Fizzsy-Lib-Operator-Modulo 'Skelp.Fizzsy.Lib.Operator.Modulo') and unused for any other. |
| negate | [System.Boolean](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Boolean 'System.Boolean') | If true will negate the output of the predicate. |

##### Exceptions

| Name | Description |
| ---- | ----------- |
| [System.ArgumentException](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.ArgumentException 'System.ArgumentException') | Thrown when `T` is not a numeric type. |
| [System.ArgumentNullException](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.ArgumentNullException 'System.ArgumentNullException') | Thrown when `opr` is equal to [Modulo](#F-Skelp-Fizzsy-Lib-Operator-Modulo 'Skelp.Fizzsy.Lib.Operator.Modulo') and `resultArg` is null. |

##### Example

Predicate to be created:

```
i =&gt; i % 3 == 0
```

Matching constructor call:

```c#
var predicateHandler = new PredicateHandler&lt;int&gt;(3, Operator.Modulo, null, false);
```

<a name='P-Skelp-Fizzsy-Lib-PredicateHandler`1-Predicate'></a>
### Predicate `property`

##### Summary

Gets the compiled [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1').

<a name='P-Skelp-Fizzsy-Lib-PredicateHandler`1-PredicateLogic'></a>
### PredicateLogic `property`

##### Summary

Gets high-level, minimal information used as a template for compiling and serializing.

<a name='M-Skelp-Fizzsy-Lib-PredicateHandler`1-CompilePredicate'></a>
### CompilePredicate() `method`

##### Summary

Compiles a [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') based on parameters passed into the constructor.

##### Parameters

This method has no parameters.

<a name='T-Skelp-Fizzsy-Lib-PredicateLogic`1'></a>
## PredicateLogic\`1 `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

Represents high-level information about a [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1').

##### Generic Types

| Name | Description |
| ---- | ----------- |
| T | The type used in the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |

<a name='M-Skelp-Fizzsy-Lib-PredicateLogic`1-#ctor-`0,Skelp-Fizzsy-Lib-Operator,System-Nullable{`0},System-Boolean-'></a>
### #ctor(rightOperand,opr,resultArg,negate) `constructor`

##### Summary

Initializes a new instance of the [PredicateLogic\`1](#T-Skelp-Fizzsy-Lib-PredicateLogic`1 'Skelp.Fizzsy.Lib.PredicateLogic`1') struct.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| rightOperand | [\`0](#T-`0 '`0') | Represents the right operand of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| opr | [Skelp.Fizzsy.Lib.Operator](#T-Skelp-Fizzsy-Lib-Operator 'Skelp.Fizzsy.Lib.Operator') | Determines the logical operator of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| resultArg | [System.Nullable{\`0}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Nullable 'System.Nullable{`0}') | Is used for the operator [Modulo](#F-Skelp-Fizzsy-Lib-Operator-Modulo 'Skelp.Fizzsy.Lib.Operator.Modulo') and unused for any other. |
| negate | [System.Boolean](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Boolean 'System.Boolean') | If true will negate the output of the predicate. |

<a name='P-Skelp-Fizzsy-Lib-PredicateLogic`1-Negate'></a>
### Negate `property`

##### Summary

Gets a value indicating whether or not to negate the output of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1').

<a name='P-Skelp-Fizzsy-Lib-PredicateLogic`1-Operator'></a>
### Operator `property`

##### Summary

Gets the logical operator of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1').

<a name='P-Skelp-Fizzsy-Lib-PredicateLogic`1-ResultArg'></a>
### ResultArg `property`

##### Summary

Gets the result used for the operator [Modulo](#F-Skelp-Fizzsy-Lib-Operator-Modulo 'Skelp.Fizzsy.Lib.Operator.Modulo') while unused for any other.

<a name='P-Skelp-Fizzsy-Lib-PredicateLogic`1-RightOperand'></a>
### RightOperand `property`

##### Summary

Gets the right operand of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1').

<a name='T-Skelp-Fizzsy-Lib-RuleLogic`1'></a>
## RuleLogic\`1 `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

Represents a combination of a [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1') and a true-value used for serialization and to initialize [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instances.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| T | The type used in the [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1'). |

<a name='M-Skelp-Fizzsy-Lib-RuleLogic`1-#ctor-Skelp-Fizzsy-Lib-PredicateHandler{`0},System-String-'></a>
### #ctor(predicateHandler,trueValue) `constructor`

##### Summary

Initializes a new instance of the [RuleLogic\`1](#T-Skelp-Fizzsy-Lib-RuleLogic`1 'Skelp.Fizzsy.Lib.RuleLogic`1') struct.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| predicateHandler | [Skelp.Fizzsy.Lib.PredicateHandler{\`0}](#T-Skelp-Fizzsy-Lib-PredicateHandler{`0} 'Skelp.Fizzsy.Lib.PredicateHandler{`0}') | Any valid [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1'). |
| trueValue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Any [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String'). |

<a name='P-Skelp-Fizzsy-Lib-RuleLogic`1-PredicateHandler'></a>
### PredicateHandler `property`

##### Summary

Gets the [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1').

<a name='P-Skelp-Fizzsy-Lib-RuleLogic`1-TrueValue'></a>
### TrueValue `property`

##### Summary

Gets the [TrueValue](#P-Skelp-Fizzsy-Lib-RuleLogic`1-TrueValue 'Skelp.Fizzsy.Lib.RuleLogic`1.TrueValue') used when [](#!-PredicateHandler-Predicate 'PredicateHandler.Predicate') returns true.

<a name='T-Skelp-Fizzsy-Lib-RuleSet`1'></a>
## RuleSet\`1 `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

Represents an abstract container for instances of type [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') which in turn can be used to process Fizzsy logic.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| T | Any numeric type. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-#ctor'></a>
### #ctor() `constructor`

##### Summary

Initializes a new instance of the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') class.

##### Parameters

This constructor has no parameters.

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-#ctor-System-Collections-Generic-IEnumerable{Skelp-Fizzsy-Lib-RuleLogic{`0}}-'></a>
### #ctor(predicateHandlers) `constructor`

##### Summary

Initializes a new instance of the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') classs and compiles a [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') for each item in `predicateHandlers`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| predicateHandlers | [System.Collections.Generic.IEnumerable{Skelp.Fizzsy.Lib.RuleLogic{\`0}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Collections.Generic.IEnumerable 'System.Collections.Generic.IEnumerable{Skelp.Fizzsy.Lib.RuleLogic{`0}}') | Any collection of [RuleLogic\`1](#T-Skelp-Fizzsy-Lib-RuleLogic`1 'Skelp.Fizzsy.Lib.RuleLogic`1') where no item is null. |

<a name='P-Skelp-Fizzsy-Lib-RuleSet`1-Item-System-Int32-'></a>
### Item `property`

##### Summary

Gets or sets a [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1').

##### Returns

Returns the [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') corresponding to the `index`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| index | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | Any positive integer. |

<a name='P-Skelp-Fizzsy-Lib-RuleSet`1-RuleLogics'></a>
### RuleLogics `property`

##### Summary

Gets or sets the ordered collection of [RuleLogic\`1](#T-Skelp-Fizzsy-Lib-RuleLogic`1 'Skelp.Fizzsy.Lib.RuleLogic`1') instances.

<a name='P-Skelp-Fizzsy-Lib-RuleSet`1-Rules'></a>
### Rules `property`

##### Summary

Gets or sets the ordered collection of [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instances.

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-Add-`0,Skelp-Fizzsy-Lib-Operator,System-Nullable{`0},System-Boolean,System-String-'></a>
### Add(rightOperand,opr,resultArg,negate,trueValue) `method`

##### Summary

Initializes a new [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') and adds it to the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') instance.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| rightOperand | [\`0](#T-`0 '`0') | Represents the right operand of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| opr | [Skelp.Fizzsy.Lib.Operator](#T-Skelp-Fizzsy-Lib-Operator 'Skelp.Fizzsy.Lib.Operator') | Determines the logical operator of the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| resultArg | [System.Nullable{\`0}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Nullable 'System.Nullable{`0}') | Is used for the operator [Modulo](#F-Skelp-Fizzsy-Lib-Operator-Modulo 'Skelp.Fizzsy.Lib.Operator.Modulo') and unused for any other. |
| negate | [System.Boolean](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Boolean 'System.Boolean') | If true will negate the output of the predicate. |
| trueValue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Any [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') which will be returned when the corresponding [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') returns true. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-Add-Skelp-Fizzsy-Lib-PredicateHandler{`0},System-String-'></a>
### Add(predicateHandler,trueValue) `method`

##### Summary

Initializes a new [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') and adds it to the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') instance.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| predicateHandler | [Skelp.Fizzsy.Lib.PredicateHandler{\`0}](#T-Skelp-Fizzsy-Lib-PredicateHandler{`0} 'Skelp.Fizzsy.Lib.PredicateHandler{`0}') | Any valid instance of a [PredicateHandler\`1](#T-Skelp-Fizzsy-Lib-PredicateHandler`1 'Skelp.Fizzsy.Lib.PredicateHandler`1') class. |
| trueValue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Any [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') which will be returned when the corresponding [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') returns true. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-ConvertToRule-Skelp-Fizzsy-Lib-RuleLogic{`0}-'></a>
### ConvertToRule(ruleLogic) `method`

##### Summary

Takes a [RuleLogic\`1](#T-Skelp-Fizzsy-Lib-RuleLogic`1 'Skelp.Fizzsy.Lib.RuleLogic`1') instance as a base to create a [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1').

##### Returns

Returns a new [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1').

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| ruleLogic | [Skelp.Fizzsy.Lib.RuleLogic{\`0}](#T-Skelp-Fizzsy-Lib-RuleLogic{`0} 'Skelp.Fizzsy.Lib.RuleLogic{`0}') | Any valid [RuleLogic\`1](#T-Skelp-Fizzsy-Lib-RuleLogic`1 'Skelp.Fizzsy.Lib.RuleLogic`1'). |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-Deserialize-System-Byte[]-'></a>
### Deserialize(data) `method`

##### Summary

Takes a [Byte](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Byte 'System.Byte') array containing a serialized [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') and creates a new, deserialized instance.

##### Returns

Returns a new, deserialized [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') instance.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| data | [System.Byte[]](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Byte[] 'System.Byte[]') | Any valid, serialized [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1'). |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-Evaluate-`0-'></a>
### Evaluate(value) `method`

##### Summary

Iterates all [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instances and builds a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') based on the results of each [Predicate](#P-Skelp-Fizzsy-Lib-Rule`1-Predicate 'Skelp.Fizzsy.Lib.Rule`1.Predicate') with `value` as input.

##### Returns

Returns a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') which either contains a concatenation of every [TrueValue](#P-Skelp-Fizzsy-Lib-Rule`1-TrueValue 'Skelp.Fizzsy.Lib.Rule`1.TrueValue') where its [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true,
or, if no [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, will return `value`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| value | [\`0](#T-`0 '`0') | Any valid `T` instance. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-EvaluateMany-`0[]-'></a>
### EvaluateMany(values) `method`

##### Summary

Iterates all [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instances and builds a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') based on the results of each [Predicate](#P-Skelp-Fizzsy-Lib-Rule`1-Predicate 'Skelp.Fizzsy.Lib.Rule`1.Predicate') with `values` as input.

##### Returns

Returns a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') array where each [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') either contains a concatenation of every [TrueValue](#P-Skelp-Fizzsy-Lib-Rule`1-TrueValue 'Skelp.Fizzsy.Lib.Rule`1.TrueValue')
where its [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, or, if no [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, will return the corresponding item in `values`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| values | [\`0[]](#T-`0[] '`0[]') | Any valid collection of `T` instances. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-EvaluateMany-System-Collections-Generic-IEnumerable{`0}-'></a>
### EvaluateMany(values) `method`

##### Summary

Iterates all [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instances and builds a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') based on the results of each [Predicate](#P-Skelp-Fizzsy-Lib-Rule`1-Predicate 'Skelp.Fizzsy.Lib.Rule`1.Predicate') with `values` as input.

##### Returns

Returns a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') array where each [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') either contains a concatenation of every [TrueValue](#P-Skelp-Fizzsy-Lib-Rule`1-TrueValue 'Skelp.Fizzsy.Lib.Rule`1.TrueValue')
where its [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, or, if no [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, will return the corresponding item in `values`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| values | [System.Collections.Generic.IEnumerable{\`0}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Collections.Generic.IEnumerable 'System.Collections.Generic.IEnumerable{`0}') | Any valid collection of `T` instances. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-EvaluateMany-System-Int32,System-Int32-'></a>
### EvaluateMany(start,count) `method`

##### Summary

Iterates all [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instances and builds a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') based on the results of each [Predicate](#P-Skelp-Fizzsy-Lib-Rule`1-Predicate 'Skelp.Fizzsy.Lib.Rule`1.Predicate')
with a range of `count` numbers, starting from `start` as input.

##### Returns

Returns a [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') array where each [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') either contains a concatenation of every [TrueValue](#P-Skelp-Fizzsy-Lib-Rule`1-TrueValue 'Skelp.Fizzsy.Lib.Rule`1.TrueValue')
where its [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, or, if no [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') has returned true, will return the corresponding number.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| start | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | The inclusive number from which to start iterating. |
| count | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | The total amount of numbers to iterate. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-GetEnumerator'></a>
### GetEnumerator() `method`

##### Summary

Gets the Enumerator for the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') class.

##### Returns

Returns the Enumerator for the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') class.

##### Parameters

This method has no parameters.

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-Move-System-Int32,System-Int32-'></a>
### Move(oldIndex,newIndex) `method`

##### Summary

Moves a [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instance with an index of `newIndex` to `oldIndex`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| oldIndex | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | The index of the [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instance to be moved. |
| newIndex | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | The index where the [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') instance is supposed to be moved to. |

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-Serialize'></a>
### Serialize() `method`

##### Summary

Serializes the current [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') instance into a [Byte](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Byte 'System.Byte') array.

##### Returns

Returns a [Byte](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Byte 'System.Byte') array containing a serialized [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') instance.

##### Parameters

This method has no parameters.

<a name='M-Skelp-Fizzsy-Lib-RuleSet`1-System#Collections#IEnumerable#GetEnumerator'></a>
### System#Collections#IEnumerable#GetEnumerator() `method`

##### Summary

Gets the Enumerator for the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') class.

##### Returns

Returns the Enumerator for the [RuleSet\`1](#T-Skelp-Fizzsy-Lib-RuleSet`1 'Skelp.Fizzsy.Lib.RuleSet`1') class.

##### Parameters

This method has no parameters.

<a name='T-Skelp-Fizzsy-Lib-Rule`1'></a>
## Rule\`1 `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

Represents a combination of a [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1') and a true-value.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| T | The type used in the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |

<a name='M-Skelp-Fizzsy-Lib-Rule`1-#ctor-System-Predicate{`0},System-String-'></a>
### #ctor(predicate,trueValue) `constructor`

##### Summary

Initializes a new instance of the [Rule\`1](#T-Skelp-Fizzsy-Lib-Rule`1 'Skelp.Fizzsy.Lib.Rule`1') struct.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| predicate | [System.Predicate{\`0}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate 'System.Predicate{`0}') | Any valid [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1'). |
| trueValue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Any [String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String'). |

<a name='P-Skelp-Fizzsy-Lib-Rule`1-Predicate'></a>
### Predicate `property`

##### Summary

Gets the [Predicate\`1](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Predicate`1 'System.Predicate`1').

<a name='P-Skelp-Fizzsy-Lib-Rule`1-TrueValue'></a>
### TrueValue `property`

##### Summary

Gets the [TrueValue](#P-Skelp-Fizzsy-Lib-Rule`1-TrueValue 'Skelp.Fizzsy.Lib.Rule`1.TrueValue') used when [Predicate](#P-Skelp-Fizzsy-Lib-Rule`1-Predicate 'Skelp.Fizzsy.Lib.Rule`1.Predicate') returns true.

<a name='T-Skelp-Fizzsy-Lib-TypeExtentions'></a>
## TypeExtentions `type`

##### Namespace

Skelp.Fizzsy.Lib

##### Summary

Contains extention methods for the [Type](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Type 'System.Type') class.

<a name='M-Skelp-Fizzsy-Lib-TypeExtentions-IsNumber-System-Type-'></a>
### IsNumber(type) `method`

##### Summary

Ensures the given `type` is a number.

##### Returns

Returns true if `type` is a number, returns false otherwise.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| type | [System.Type](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Type 'System.Type') | Any [Type](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Type 'System.Type'). |
