# Fizzsy
This repository exists to showcase how complex and dynamic a simple project such as the infamous 'FizzBuzz' interview test can become. This is possibly the most dynamic FizzBuzz can get.

You will find a FizzsyLib, containing all the actual logic, and a FizzsyApp, a console application to showcase most of the flexibility the library offers.

For instance, you have a couple of operators, instead of just modulo, to choose from when constructing the rules of the game, check out the documentation below for more.
It is also possible to serialize and deserialize rule sets. Examples are in the documentation section below.

## Documentation

### Full class library documentation
To get to the auto-generated FizzsyLib class library documentation, [click here](FizzsyLib/FizzsyLib.md).

### Short guide
The most simple, excecutable implementation of the FizzsyLib class library is done with the following lines of C# code.
The code below will create a new rule set and populate it with the most common FizzBuzz rules, evaluates numbers 1 through 100 inclusively and finally writes each result on the console.

```cs
var ruleSet = new RuleSet<int>();

ruleSet.Add(3, Operator.Modulo, 0, false, "Fizz");
ruleSet.Add(5, Operator.Modulo, 0, false, "Buzz");

string[] results = ruleSet.EvaluateMany(1, 100);

foreach (var result in results)
{
	Console.WriteLine(result);
}
```

To serialize, save a ```RuleSet``` as a file and then deserialize it from the file, the following lines can be called.

```cs
var ruleSet = new RuleSet<int>();
...
byte[] data = ruleSet.Serialize();
File.WriteAllBytes("ruleSet.fizz", data);
```
```cs
byte[] data = File.ReadAllBytes("ruleSet.fizz");
var ruleSet = RuleSet<int>.Deserialize(data);
```