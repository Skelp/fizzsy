﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Skelp.Fizzsy.Lib;

namespace Skelp.Fizzsy.App
{
    class Program
    {
        private static RuleSet<int> Rules { get; set; }

        private static Regex regex = new Regex(@"(?<var>\w+) *(?<operator>[%<>=][=]?) *(?<rightOperand>\d+([\.,]\d+)?) *(?<negate>!)?=? *(?<result>\d+([\.,]\d+)?)?", RegexOptions.Compiled);

        private static void Main(string[] args)
        {
            while (true)
            {
                Rules = null;

                SuggestLoading();

                if (Rules == null)
                {
                    Rules = new RuleSet<int>();
                    CreateRuleSet();
                }

                GetRange(out int start, out int count);
                var results = Rules.EvaluateMany(start, count);
                PrintResults(results);
                Console.ReadLine();
                Console.Clear();
            }
        }

        private static void SuggestLoading()
        {
            var files = Directory.GetFiles(@".\RuleSets", "*.fizz");

            var suggestionText = "Choose rule set: ";

            foreach (var file in files)
            {
                suggestionText += $"'{Path.GetFileNameWithoutExtension(file)}', ";
            }

            suggestionText += "leave empty to create new.";

            Console.WriteLine(suggestionText);

            bool validInput = false;

            while (validInput == false)
            {
                var input = Console.ReadLine();

                try
                {
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        validInput = true;
                        break;
                    }

                    input = input.Trim().ToLower();
                    var file = files.FirstOrDefault(x => x.ToLower().EndsWith($"{input}.fizz"));

                    if (string.IsNullOrWhiteSpace(file))
                    {
                        continue;
                    }

                    Rules = RuleSet<int>.Deserialize(File.ReadAllBytes(file));
                    validInput = true;
                }
                catch
                {
                    Console.WriteLine("Invalid input or rule set corrupted! Try again.");
                }
            }
        }

        private static void CreateRuleSet()
        {
            bool addRule = true;

            while (addRule)
            {
                Console.WriteLine("Please enter a condition / predicate");
                Console.WriteLine("Example: i % 3 = 0");
                var innerInput = string.Empty;

                PredicateHandler<int> predicateHandler = null;

                bool innerValidInput = false;

                while (innerValidInput == false)
                {
                    innerInput = Console.ReadLine();

                    try
                    {
                        predicateHandler = ParseToPredicateHandler<int>(innerInput);
                        innerValidInput = true;
                    }
                    catch
                    {
                        Console.WriteLine("Invalid input! Try again.");
                    }
                }

                Console.WriteLine("Success! Please enter the text to be displayed when the condition holds true:");
                var trueValue = Console.ReadLine();
                Rules.Add(predicateHandler, trueValue);

                Console.WriteLine($"Successfully added '{innerInput}' => '{trueValue}'!");
                Console.WriteLine("Would you like to add more rules? [Y/N]");
                var key = default(ConsoleKeyInfo);

                var validKey = false;

                while (validKey == false)
                {
                    key = Console.ReadKey(true);

                    switch (key.Key)
                    {
                        case ConsoleKey.Y:
                            validKey = true;
                            break;
                        case ConsoleKey.N:
                            validKey = true;
                            addRule = false;
                            break;
                        default:
                            break;
                    }
                }
            }

            Console.WriteLine("Give the rule set a name:");
            var input = string.Empty;

            var validInput = false;

            while (validInput == false)
            {
                input = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Invalid input! Try again.");
                    continue;
                }

                validInput = true;
            }

            var bytes = Rules.Serialize();

            File.WriteAllBytes($@".\RuleSets\{input}.fizz", bytes);
            Console.WriteLine($"Done! Saved rule set '{input}'.");
        }

        private static void GetRange(out int start, out int count)
        {
            start = 0;
            count = 0;

            Console.WriteLine("What number would you like to start on?");
            var input = string.Empty;

            bool validInput = false;

            while (validInput == false)
            {
                input = Console.ReadLine();

                try
                {
                    start = int.Parse(input);
                    validInput = true;
                }
                catch
                {
                    Console.WriteLine("Invalid input! Try again.");
                }
            }

            Console.WriteLine("How many numbers should be evaluated?");
            input = string.Empty;

            validInput = false;

            while (validInput == false)
            {
                input = Console.ReadLine();

                try
                {
                    count = int.Parse(input);
                    validInput = true;
                }
                catch
                {
                    Console.WriteLine("Invalid input! Try again.");
                }
            }
        }

        private static void PrintResults(string[] results)
        {
            for (int i = 0; i < results.Length; i++)
            {
                Console.WriteLine(results[i]);
            }
        }

        private static PredicateHandler<T> ParseToPredicateHandler<T>(string input)
            where T : struct
        {
            input = input.Trim();
            var results = regex.Match(input).Groups;

            var rightOperand = (T)Convert.ChangeType(results["rightOperand"].Value, typeof(T));
            var @operator = ParseOperator(results["operator"].Value);
            var negate = results["negate"].Success;
            T resultArg = default;

            if (@operator == Operator.Modulo)
            {
                resultArg = (T)Convert.ChangeType(results["result"].Value, typeof(T));
            }

            return new PredicateHandler<T>(rightOperand, @operator, resultArg, negate);
        }

        private static Operator ParseOperator(string input)
        {
            switch (input)
            {
                case "=":
                    return Operator.Equals;
                case "%":
                    return Operator.Modulo;
                case ">":
                    return Operator.GreaterThan;
                case "<":
                    return Operator.LessThan;
                case ">=":
                    return Operator.GreaterThanOrEqual;
                case "<=":
                    return Operator.LessThanOrEqual;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}